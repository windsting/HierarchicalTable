# A test project


[![build status](https://gitlab.com/windsting/HierarchicalTable/badges/master/build.svg)](https://gitlab.com/windsting/HierarchicalTable/commits/master)

Content comes from:

- [Storing Hierarchical Data in a Database](https://www.sitepoint.com/hierarchical-data-database/)
- [C#: Building a Useful, Extensible .NET Console Application Template for Development and Testing](https://www.codeproject.com/Articles/816301/Csharp-Building-a-Useful-Extensible-NET-Console-Ap)

hopfully, ended with a reusable library, or even better, a package.
