﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp.SQLite
{
    public class BloggingContext : DbContext
    {
        public DbSet<Blog> Blogs { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<PTreeItem> PTreeItems { get; set; }
        public DbSet<TreeItem> TreeItems { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder option)
        {
            option.UseSqlite("Data Source=blogging.db");
        }

        public void EnsureData()
        {
            this.EnsureTreeItems();
            this.EnsurePTreeItemTable();
        }
    }

    public class Post
    {
        public int PostId { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }

        public int BlogId { get; set; }
        public Blog Blog { get; set; }
    }

    public class Blog
    {
        public int BlogId { get; set; }
        public string Url { get; set; }

        public List<Post> Posts { get; set; }
    }

    //  Tree Item with ParentId
    public class PTreeItem
    {
        public long PTreeItemId { get; set; }
        public string Name { get; set; }
        public long ParentId { get; set; }

    }

    public class TreeItem
    {
        public long TreeItemId { get; set; }
        public string Name { get; set; }
        public long Lft { get; set; }
        public long Rgt { get; set; }
    }
}
