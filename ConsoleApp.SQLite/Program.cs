﻿using System;

namespace ConsoleApp.SQLite
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var db = new BloggingContext())
            {
                db.EnsureData();
                Console.WriteLine("All blogs in database:");
                foreach(var blog in db.Blogs)
                {
                    Console.WriteLine($" - {blog.Url}");
                }

                db.PrintPTree();
                db.PrintPath("Banana");

                db.TreeItems.PrintTree("Food");
                db.TreeItems.PrintPath("Banana");
            }
        }
    }
}