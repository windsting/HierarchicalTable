﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace ConsoleApp.SQLite
{
    public static class TreeOperate
    {
        public static void PrintTree(this DbSet<TreeItem> set, string name)
        {
            var item = set.FirstOrDefault(i => i.Name == name);
            if(item == null)
            {
                Console.WriteLine($"PrintTree no record found for invalid name:{name}.");
                return;
            }

            var children = set.Where(i => i.Lft >= item.Lft && i.Lft < item.Rgt)
                                .OrderBy(i => i.Lft)
                                .ToList();
            List<long> right = new List<long>();
            foreach (var child in children)
            {
                while (right.Count > 0 && right[right.Count - 1] < child.Rgt)
                {
                    right.RemoveAt(right.Count - 1);
                }

                Console.WriteLine($"{PTreeOperate.Spaces(right.Count)}[{child.TreeItemId}]{child.Name}");

                right.Add(child.Rgt);
            }
        }

        public static void PrintPath(this DbSet<TreeItem> set, string name)
        {
            var item = set.FirstOrDefault(i => i.Name == name);
            if (item == null)
            {
                Console.WriteLine($"PrintPath no record found for invalid name:{name}.");
                return;
            }

            var ancestors = set.Where(i => i.Lft < item.Lft && i.Rgt > item.Rgt)
                                .OrderBy(i => i.Lft)
                                .ToList();

            Console.WriteLine($"\nancestors of {name}:");
            for (int i = 0; i < ancestors.Count; ++i)
            {
                var node = ancestors[i];
                Console.WriteLine($"\t[{i}]{node.Name}");
            }
        }

        public static void ImpportParentTreeTable(this BloggingContext db,
                                                DbSet<TreeItem> set,
                                                DbSet<PTreeItem> pset,
                                                Func<PTreeItem, PTreeItem, bool> isChildOf,
                                                Func<PTreeItem, TreeItem> itemFromPItem,
                                                Func<PTreeItem, long> getParentId,
                                                long PTableRootParentID)
        {
            var roots = pset
                        .Where(p => getParentId(p) == PTableRootParentID)
                        .ToList();
            if(roots.Count > 1)
            {
                var rootTree = new TreeItem { Lft = 1, Rgt = 2 };
                set.Add(rootTree);
                long right = 2;
                foreach(var root in roots)
                {
                    right = set.ImportParentTree(pset, isChildOf, root, itemFromPItem, right);
                }
                rootTree.Rgt = right;
            }
            else if(roots.Count == 1)
            {
                var right = set.ImportParentTree(pset, isChildOf, roots[0], itemFromPItem, 1);
            }
            else
            {
                Console.WriteLine($"ImpportParentTreeTable no root node found, invalid PTableRootParentID({PTableRootParentID})?");
            }
            db.SaveChanges();
        }

        public static long ImportParentTree(this DbSet<TreeItem> set,
                                                DbSet<PTreeItem> pset,
                                                Func<PTreeItem, PTreeItem, bool> isChildOf,
                                                PTreeItem pnode,
                                                Func<PTreeItem, TreeItem> itemFromPItem,
                                                long left)
        {
            var right = left + 1;
            var plist = pset
                .Where(pi => isChildOf(pi, pnode))
                .ToList();

            foreach(var child in plist)
            {
                right = ImportParentTree(set, pset, isChildOf, child, itemFromPItem, right);
            }

            var item = itemFromPItem(pnode);
            item.Lft = left;
            item.Rgt = right;
            set.Add(item);
            item.TreeItemId = pnode.PTreeItemId;

            return right + 1;
        }


        public static void EnsureTreeItems(this BloggingContext db)
        {
            var set = db.TreeItems;

            if (set.Any())
                return;

            db.ImpportParentTreeTable(db.TreeItems,
                                        db.PTreeItems,
                                        (a, b) => a.ParentId == b.PTreeItemId,
                                        (p) => new TreeItem { TreeItemId = p.PTreeItemId, Name = p.Name },
                                        (p) => p.ParentId,
                                        0);

            //AddTestDatas(set);

            db.SaveChanges();
        }

        private static void AddTestDatas(DbSet<TreeItem> set)
        {
            set.Add(new TreeItem { Name = "Food", Lft = 1, Rgt = 18 });
            set.Add(new TreeItem { Name = "Fruit", Lft = 2, Rgt = 11 });
            set.Add(new TreeItem { Name = "Red", Lft = 3, Rgt = 6 });
            set.Add(new TreeItem { Name = "Cherry", Lft = 4, Rgt = 5 });
            set.Add(new TreeItem { Name = "Yellow", Lft = 7, Rgt = 10 });
            set.Add(new TreeItem { Name = "Banana", Lft = 8, Rgt = 9 });
            set.Add(new TreeItem { Name = "Meat", Lft = 12, Rgt = 17 });
            set.Add(new TreeItem { Name = "Beef", Lft = 13, Rgt = 14 });
            set.Add(new TreeItem { Name = "Pork", Lft = 15, Rgt = 16 });
        }
    }
}
