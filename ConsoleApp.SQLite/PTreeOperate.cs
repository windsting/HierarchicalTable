﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace ConsoleApp.SQLite
{
    public static class PTreeOperate
    {
        public static int AddPTreeItem(this BloggingContext db, string name, long parentId)
        {
            db.PTreeItems.Add(new PTreeItem { Name = name, ParentId = parentId });
            var count = db.SaveChanges();
            return count;
        }

        public static int AddPTreeItem(this BloggingContext db, string name, string parentName)
        {
            var parent = db.PTreeItems.FirstOrDefault(i => i.Name == parentName);
            if (parent != null)
            {
                return db.AddPTreeItem(name, parent.PTreeItemId);
            }

            Console.WriteLine($"Invalid parentName:{parentName}");
            return 0;
        }

        public static PTreeItem RootItem(this DbSet<PTreeItem> set)
        {
            return set.FirstOrDefault(i => i.ParentId == 0);
        }

        public static void PrintPTree(this BloggingContext db)
        {
            var item = db.PTreeItems.RootItem();
            Console.WriteLine($"{"\n"}[{item.PTreeItemId}]{item.Name}");
            db.PrintChildren(1, item.PTreeItemId);
        }

        public static void PrintChildren(this BloggingContext db, int depth, long parentId)
        {
            var children = db.PTreeItems
                            .Where(i => i.ParentId == parentId && i.ParentId != i.PTreeItemId)
                            .ToList();
            var spaces = Spaces(depth);
            foreach (var child in children)
            {
                Console.WriteLine($"{spaces}[{child.PTreeItemId}]{child.Name}");
                db.PrintChildren(depth + 1, child.PTreeItemId);
            }
        }

        public static void PrintPath(this BloggingContext db, string name)
        {
            var path = db.PathToNode(name);
            Console.WriteLine($"\nancestors of {name}:");
            for (int i = 0; i < path.Count; ++i)
            {
                var node = path[i];
                Console.WriteLine($"\t[{i}] {node.Name}");
            }
        }

        public static IList<PTreeItem> PathToNode(this BloggingContext db, string name)
        {
            var item = db.PTreeItems.FirstOrDefault(i => i.Name == name);
            List<PTreeItem> path = new List<PTreeItem>();
            while (item.ParentId != item.PTreeItemId)
            {
                var itemName = item.Name;
                var pid = item.ParentId;
                item = db.PTreeItems.FirstOrDefault(i => i.PTreeItemId == pid);
                if (item == null)
                {
                    Console.WriteLine($"root node found: {itemName}.");
                    break;
                }
                path.Add(item);
            }

            path.Reverse();
            return path;
        }

        const char DummyChar = ' ';
        const int CharCountPerLevel = 2;
        public static string Spaces(int n)
        {
            var charCount = n * CharCountPerLevel;
            return new string(DummyChar, charCount);
        }

        public static void EnsurePTreeItemTable(this BloggingContext db)
        {
            if (db.PTreeItems.Any())
                return;

            db.AddPTreeItem("Food", 0);

            db.AddPTreeItem("Fruit", "Food");
            db.AddPTreeItem("Red", "Fruit");
            db.AddPTreeItem("Yellow", "Fruit");
            db.AddPTreeItem("Green", "Fruit");

            db.AddPTreeItem("Cherry", "Red");
            db.AddPTreeItem("Apple", "Red");
            db.AddPTreeItem("Banana", "Yellow");
            db.AddPTreeItem("Watermelon", "Green");

            db.AddPTreeItem("Meat", "Food");
            db.AddPTreeItem("Beef", "Meat");
            db.AddPTreeItem("Pork", "Meat");
            db.AddPTreeItem("Lamb", "Meat");


            db.Blogs.Add(new Blog { Url = "http://blogs.msdn.com/netcore" });
            var count = db.SaveChanges();
            Console.WriteLine($"{count} records saved to database\n");
        }
    }
}
