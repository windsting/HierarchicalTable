﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ConsoleApp.SQLite.Migrations
{
    public partial class PTreeItemReal : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PTreeItems",
                columns: table => new
                {
                    PTreeItemId = table.Column<long>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(nullable: true),
                    ParentId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PTreeItems", x => x.PTreeItemId);
                    table.ForeignKey(
                        name: "FK_PTreeItems_PTreeItems_ParentId",
                        column: x => x.ParentId,
                        principalTable: "PTreeItems",
                        principalColumn: "PTreeItemId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PTreeItems_ParentId",
                table: "PTreeItems",
                column: "ParentId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PTreeItems");
        }
    }
}
