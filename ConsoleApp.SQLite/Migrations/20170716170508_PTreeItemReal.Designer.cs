﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using ConsoleApp.SQLite;

namespace ConsoleApp.SQLite.Migrations
{
    [DbContext(typeof(BloggingContext))]
    [Migration("20170716170508_PTreeItemReal")]
    partial class PTreeItemReal
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.2");

            modelBuilder.Entity("ConsoleApp.SQLite.Blog", b =>
                {
                    b.Property<int>("BlogId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Url");

                    b.HasKey("BlogId");

                    b.ToTable("Blogs");
                });

            modelBuilder.Entity("ConsoleApp.SQLite.Post", b =>
                {
                    b.Property<int>("PostId")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("BlogId");

                    b.Property<string>("Content");

                    b.Property<string>("Title");

                    b.HasKey("PostId");

                    b.HasIndex("BlogId");

                    b.ToTable("Posts");
                });

            modelBuilder.Entity("ConsoleApp.SQLite.PTreeItem", b =>
                {
                    b.Property<long>("PTreeItemId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.Property<long>("ParentId");

                    b.HasKey("PTreeItemId");

                    b.HasIndex("ParentId");

                    b.ToTable("PTreeItems");
                });

            modelBuilder.Entity("ConsoleApp.SQLite.Post", b =>
                {
                    b.HasOne("ConsoleApp.SQLite.Blog", "Blog")
                        .WithMany("Posts")
                        .HasForeignKey("BlogId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("ConsoleApp.SQLite.PTreeItem", b =>
                {
                    b.HasOne("ConsoleApp.SQLite.PTreeItem", "Parent")
                        .WithMany()
                        .HasForeignKey("ParentId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
