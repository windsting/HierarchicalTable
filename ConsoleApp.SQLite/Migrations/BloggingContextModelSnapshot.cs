﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using ConsoleApp.SQLite;

namespace ConsoleApp.SQLite.Migrations
{
    [DbContext(typeof(BloggingContext))]
    partial class BloggingContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.2");

            modelBuilder.Entity("ConsoleApp.SQLite.Blog", b =>
                {
                    b.Property<int>("BlogId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Url");

                    b.HasKey("BlogId");

                    b.ToTable("Blogs");
                });

            modelBuilder.Entity("ConsoleApp.SQLite.Post", b =>
                {
                    b.Property<int>("PostId")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("BlogId");

                    b.Property<string>("Content");

                    b.Property<string>("Title");

                    b.HasKey("PostId");

                    b.HasIndex("BlogId");

                    b.ToTable("Posts");
                });

            modelBuilder.Entity("ConsoleApp.SQLite.PTreeItem", b =>
                {
                    b.Property<long>("PTreeItemId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.Property<long>("ParentId");

                    b.HasKey("PTreeItemId");

                    b.ToTable("PTreeItems");
                });

            modelBuilder.Entity("ConsoleApp.SQLite.TreeItem", b =>
                {
                    b.Property<long>("TreeItemId")
                        .ValueGeneratedOnAdd();

                    b.Property<long>("Lft");

                    b.Property<string>("Name");

                    b.Property<long>("Rgt");

                    b.HasKey("TreeItemId");

                    b.ToTable("TreeItems");
                });

            modelBuilder.Entity("ConsoleApp.SQLite.Post", b =>
                {
                    b.HasOne("ConsoleApp.SQLite.Blog", "Blog")
                        .WithMany("Posts")
                        .HasForeignKey("BlogId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
